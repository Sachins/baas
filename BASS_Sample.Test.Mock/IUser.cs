﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BASS_Sample.Test.Mock
{
   public class IUser
    {
        public string Gender { get; set; }
        public string Name { get; set; }
        public string OrgName { get; set; }
    }
}
