﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BASS_Sample.Test.Mock
{
   public class UserDetails
    {
        public UserDetails()
        {

        }

        public Mock<IUser> GetDetails()
        {
            var mocInfo=new Mock<IUser>();
            mocInfo.Object.Name = "Sachin";
            mocInfo.Object.Gender = "Male";
            mocInfo.Object.OrgName = "Nagarro";
            return  mocInfo;
        }
    }
}
