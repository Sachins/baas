﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace BASS_Sample.UITest
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;
        int count = 0;
        int newCount = -1;
        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test, Order(1)]
        public void WelcomeTextIsDisplayed()
        {
            AppResult[] results = app.WaitForElement(c => c.Marked("Firebase Records"));
            app.Screenshot("Welcome screen.");
            Assert.IsTrue(results.Any());
        }

        [Test,Order(2)]
        [TestCase("TestName", "TestOrg")]
        public void AddNewRecordTest(string name,string org)
        {          
            //Arrange
            app.EnterText("Name", name);
            app.EnterText("Organization", org);
            app.Tap(x => x.Marked("Select Gender"));           
            for (int i = 0; i < 5; i++)
            {
                if (app.Query(m => m.Text("Male")).Length == 0)
                    app.ScrollDown(x => x.Id("UserList"));
                else
                {
                    app.Tap(x => x.Text("Male"));
                    break;
                }
            }
            Assert.IsTrue(app.Query("GenderPicker")[0].Text == "Male");
            //Action
            app.Tap("Save");         
            var appresult = app.Query(x => x.Id("UserList").Child()).FirstOrDefault(c=>c.Text== "Mr. TestName from TestOrg");           
            Assert.IsTrue(true);
        }        
    }
}
