﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting; 
using BASS_Sample.ViewModel;
using BASS_Sample.Test.Mock;
using Moq;

namespace BASS_Sample.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Gender_test()
        {
            var mockInfo =  new UserDetails().GetDetails();

               //Arrange
               var vm = new MainPageViewModel();
            vm.SelectedGender = mockInfo.Object.Gender;
                vm.Name = mockInfo.Object.Name;
            vm.OrgName = mockInfo.Object.OrgName; 
            //Action
            vm.CommandSave.Execute(null);
            //Assert
            Assert.AreEqual("Mr.", vm.abbrevation, "Invalid Result");
        }
    }
}
